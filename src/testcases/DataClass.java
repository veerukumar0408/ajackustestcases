package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DataClass {
	
	WebDriver driver;
	
	By SignupBtn = By.xpath("/html/body/header/div[3]/div[3]/a[2]");
	
	By username = By.name("email");
	By password = By.name("password");
	By submit = By.xpath("/html/body/app-root/app-marqetmap-app/app-map/app-popup/app-signup/div/div/perfect-scrollbar/div/div[1]/div/div[1]/form/div[6]/div");
	
	 public  DataClass(WebDriver driver) {
		 this.driver = driver;
		 
	 }
	 
	 
	 public void Signupbutton() {
		 driver.findElement(SignupBtn).click();
	 }
	 
	 public void typeUserName() {
		 
		 driver.findElement(username).sendKeys("testpro@yopmail.com");
	 
	 }
	 public void typePassword() {
	
		 driver.findElement(password).sendKeys("Qwerty@123");
		 
	 }
	 public void clickonLogin() {
		 
		 driver.findElement(submit).click();
		 
	 }

}
