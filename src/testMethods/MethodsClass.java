package testMethods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import testcases.DataClass;

public class MethodsClass {

	@Test
	
	public void verifylogin() {
		System.setProperty("webdriver.chrome.driver", "/home/delta/Documents/eclipse/chromedriver_linux64/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.propertycapsule.com/");
		
		DataClass Data = new DataClass(driver);
		
		Data.Signupbutton();
		
		Data.typeUserName();
		Data.typePassword();
		Data.clickonLogin();
		
		driver.quit();
		
	}
}
